extends Area2D

func _on_Coin_body_entered(body):
	if body.has_method("collect"):
		$AnimationPlayer.play("Collect")
		yield(get_tree().create_timer(0.7), "timeout")
		body.collect()
		queue_free()

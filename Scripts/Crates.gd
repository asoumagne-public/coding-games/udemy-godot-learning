extends Area2D

var coinScene = preload("res://Prefabs/Coin/Coin.tscn")

func _on_Crates_area_entered(area):
	if area.name == "Sword":
		$AnimationPlayer.play("Slashed")
		yield(get_node("AnimationPlayer"), "animation_finished")
		$AnimationPlayer.play("Destroyed")
		yield(get_node("AnimationPlayer"), "animation_finished")
		onDestroyed()

func onDestroyed():
	var coin = coinScene.instance()
	coin.global_position = global_position
	coin.global_scale.x = 0.5
	coin.global_scale.y = 0.5
	get_tree().get_root().add_child(coin)

extends KinematicBody2D

var speed = 40
var velocity = Vector2.ZERO
var direction = Vector2.RIGHT
var gravity = 500

func _ready():
	$PathDetector.connect("area_entered", self, "onPathEntered")

func _process(delta):
	velocity.x = direction.x * speed
	velocity.y += gravity * delta
	velocity = move_and_slide(velocity, Vector2.UP)
	$Sprite.flip_h = direction.x < 0
	
func onPathEntered(_area2d):
	direction *= -1

func _on_HitBox_body_entered(body):
	print(body)
	if body.has_method("hurt"):
		body.hurt()

func _on_HitBox_area_entered(area):
	if area.name == "Sword":
		queue_free()

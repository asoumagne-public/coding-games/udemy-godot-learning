extends KinematicBody2D

var velocity = Vector2.ZERO
var speed = 250
var maxSpeed = 500
var jump = 300
var gravity = 700
var lives = 3
var coin = 0;
var isAttacking = false

func _ready():
	pass
	
func _process(delta):	
	# Substract mean that we cannot use both at the same time
	# action_strength will handle the speed based on the strength of the key (for joypad for example)
	var movement = Input.get_action_strength("ui_right") - Input.get_action_strength("ui_left")
	
	if !isAttacking:
		if movement != 0:
			velocity.x += movement * speed * delta
			$AnimatedSprite.flip_h = movement < 0
			if $AnimatedSprite.flip_h:
				$Sword.position.x = -15
			else:
				$Sword.position.x = 13
			$AnimatedSprite.play("Walk")
		elif velocity.x == 0 && velocity.y < 0:
			$AnimatedSprite.play("Jump")
		elif velocity.x == 0 && velocity.y > 30:
			$AnimatedSprite.play("Fall")
		elif movement == 0:
			velocity.x = 0
			$AnimatedSprite.play("Idle")
			
		if is_on_floor() && Input.is_action_just_pressed("ui_accept"):
			velocity.y -= jump
			$AnimatedSprite.play("Jump")
		elif velocity.y < 0 && velocity.x != 0:
			$AnimatedSprite.play("Jump")
		elif velocity.y > 30 && velocity.x != 0:
			$AnimatedSprite.play("Fall")
			
	if Input.is_action_pressed("ui_sword"):
		if !isAttacking:
			isAttacking = true
			$Sword/CollisionShape2D.disabled = false
			
			if is_on_floor() && isAttacking:
				$AnimatedSprite.play("Sword")
			elif velocity.y < 0 && velocity.x != 0:
				$AnimatedSprite.play("Sword")
			elif velocity.y > 0 && velocity.x != 0:
				$AnimatedSprite.play("Sword")
	
	velocity.y += gravity * delta
	velocity = move_and_slide(velocity, Vector2.UP)
	
func hurt():
	lives -= 1
	
	# TODO: Investigate why this does not work
	$AnimatedSprite.play()
	
	if lives == 0:
		kill()
	elif velocity.x >= 0:
		velocity.x = -70
		velocity.y = -50
	elif velocity.x <= 0:
		velocity.x = 70
		velocity.y = 50
	
func kill():
	get_tree().reload_current_scene()
		
func collect():
	coin += 1
	print("collected ", coin)

func _on_AnimatedSprite_animation_finished():
	if $AnimatedSprite.animation == "Sword":
		isAttacking = false
		$Sword/CollisionShape2D.disabled = true

func _on_VisibilityNotifier2D_screen_exited():
	kill()
